# GestHispania Test Symfony 4

This project is developed under Symfony 4.4.

*In order to run the project, after cloning the repository, some previous steps are needed.*

1. Download and install [Composer](https://getcomposer.org/).
2. Download and install [Xampp](https://www.apachefriends.org/download.html) for PHP 7.4.23.
3. Download and install [Symfony CLI](https://symfony.com/download) .

---

## Deploy

After the previous requirements installation, is mandatory to run the next commands from the project's root path.

1. Run **composer install** for install all vendor depen.
2. Import the database gh_db into **Xampp's phpMyAdmin**.
3. Some dummy user are being created for test purpose, could be used **test2@test** or **prueba@test**, with pass *123*, for **login** (the second's one is defined with role ROLE_ADMIN, to demo purpose for some extra functionalities not allowed for a standard registered user)
4. In addition, any new user could be registered by clicking into **Register**

---

## Running the app

After login a user, the app could:

1. Show the **User's main page**, to see or edit user's data, manage the subjects under subject list (adding them to the logged user, or creating a new subject) or remove a subject from the user (leave subject)
2. Show the **Course List** page, to manage the creation / edition / delete of a current or new Course.
3. Show the **Subject List** page, to manage the creation / edition / delete of a current or new Subject.

---
