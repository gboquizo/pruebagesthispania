<?php

namespace App\Controller;

use App\Entity\Subject;
use App\Form\SubjectType;
use App\Repository\CourseRepository;
use App\Repository\SubjectRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SubjectController extends AbstractController
{
    /**
     * @Route("/subject/list", name="subject_list")
     */
    public function index(SubjectRepository $subjectRepository, CourseRepository $courseRepository): Response
    {
        $course =  $courseRepository->findAll();
        return $this->render('subject/index.html.twig', [
            'subjects' => $subjectRepository->findSorted($course),
            'course' => $course,
        ]);
    }

    /**
     * @Route("/subject/new", name="subject_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $subject = new Subject();
        $form = $this->createForm(SubjectType::class, $subject);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($subject);
            $entityManager->flush();

            return $this->redirectToRoute('subject_list');
        }

        return $this->render('subject/new.html.twig', [
            'subject' => $subject,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/subject/list/{id}", name="subject_show", methods={"GET", "POST"})
     */
    public function show(int $id): Response
    {
        $subject = $this->getDoctrine()->getRepository(Subject::class)->find($id);
        return $this->render('subject/show.html.twig', [
            'subject' => $subject,
        ]);
    }

    /**
     * @Route("/subject/add", name="subject_course_add", methods={"GET","POST"})
     */
    public function add(Request $request): Response
    {
        $addSubject = new Subject();
        $form = $this->createForm(SubjectType::class, $addSubject);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($addSubject);
            $entityManager->flush();

            return $this->redirectToRoute('course_list');
        }

        return $this->render('subject/new.html.twig', [
            'subject' => $addSubject,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/subject/{id}/add", name="subject_user", methods={"GET","POST"})
     */
    public function addUser(int $id): Response
    {

        $user = $this->getUser();
        $registeredSubject = $this->getDoctrine()->getRepository(Subject::class)->find($id);
        $registeredSubject->addUser($user);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->persist($registeredSubject);
        $entityManager->flush();

        return $this->redirectToRoute('user_show');

    }

    /**
     * @Route("/subject/{id}/delete", name="subject_user_delete", methods={"GET","POST"})
     */
    public function deleteUser(int $id): Response
    {

        $user = $this->getUser();
        $registeredSubject = $this->getDoctrine()->getRepository(Subject::class)->find($id);
        $registeredSubject->removeUser($user);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->persist($registeredSubject);
        $entityManager->flush();

        return $this->redirectToRoute('user_show');

    }


    /**
     * @Route("/subject/{id}/edit", name="subject_edit", methods={"GET","POST"})
     */
    public function edit(Request $request,  int $id): Response
    {
        $subject = $this->getDoctrine()->getRepository(Subject::class)->find($id);
        $form = $this->createForm(SubjectType::class, $subject);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('subject_list');
        }

        return $this->render('subject/edit.html.twig', [
            'subject' => $subject,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/subject/{id}/edit/delete", name="subject_delete",methods={"DELETE"})
     */
    public function delete(Request $request, Subject $subject): Response
    {
        if ($this->isCsrfTokenValid('delete'.$subject->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($subject);
            $entityManager->flush();
        }

        return $this->redirectToRoute('subject_list');
    }

}
