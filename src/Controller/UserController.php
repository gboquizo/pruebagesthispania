<?php

namespace App\Controller;

use App\Entity\Course;
use App\Entity\Subject;
use App\Entity\User;
use App\Form\UserType;
use App\Repository\CourseRepository;
use App\Repository\SubjectRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends AbstractController
{
    /**
     * @Route("/users/list", name="users_list", methods={"GET"})
     */
    public function index(UserRepository $userRepository): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $this->denyAccessUnlessGranted(['ROLE_ADMIN']);
        return $this->render('users/index.html.twig', ['users' => $userRepository->findAll()]);
    }

    /**
     * @Route("/user/list/{id}/find", name="user_find", methods={"GET","POST"})
     */
    public function find(SubjectRepository $subjectRepository, CourseRepository $courseRepository, int $id): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        $course =  $courseRepository->findAll();
        return $this->render('users/find.html.twig', [
            'user' => $user,
            'subjects' => $subjectRepository->findSorted($course),
            'course' => $course,
        ]);
    }

    /**
     * @Route("/user", name="user_show", methods={"GET","POST"})
     */
    public function show(SubjectRepository $subjectRepository, CourseRepository $courseRepository): Response
    {
        $user = $this->getUser();
        $course =  $courseRepository->findAll();
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        return $this->render('users/show.html.twig', [
            'subjects' => $subjectRepository->findSorted($course),
            'course' => $course,
            'user' => $user,
        ]);
    }

    /**
     * @Route("/users/list/{id}/edit", name="user_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, UserPasswordEncoderInterface $passwordEncoder, int $id): Response
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('password')->getData()
                )
            );
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_show');
        }

        return $this->render('users/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/users/list/{id}/delete", name="user_delete", methods={"DELETE"})
     */
    public function delete(Request $request, User $user): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('users_list');
    }
}
