<?php

namespace App\DataFixtures;

use App\Entity\Course;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CourseFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $course= new Course();
        $course->setLevel('1º');
        $course->setDegree('DAW');
        $course->setMonthDuration(9);
        $course->setAcademicYear(new \DateTime("2018-12-31 13:05:21"));
        $manager->persist($course);
        $manager->flush();
    }
}
