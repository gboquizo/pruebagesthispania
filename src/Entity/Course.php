<?php

namespace App\Entity;

use App\Repository\CourseRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CourseRepository::class)
 */
class Course
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $level;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $degree;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     */
    private ?int $monthDuration;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private ?\DateTimeInterface $academicYear;

    /**
     * @ORM\ManyToMany(targetEntity=Subject::class, mappedBy="course")
     * @ORM\OrderBy({"name" = "ASC"})
     */
    private Collection  $subjects;

    public function __construct()
    {
        $this->subjects = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLevel(): ?string
    {
        return $this->level;
    }

    public function setLevel(string $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getDegree(): ?string
    {
        return $this->degree;
    }

    public function setDegree(?string $degree): self
    {
        $this->degree = $degree;

        return $this;
    }

    public function getMonthDuration(): ?int
    {
        return $this->monthDuration;
    }

    public function setMonthDuration(?int $monthDuration): self
    {
        $this->monthDuration = $monthDuration;

        return $this;
    }

    public function getAcademicYear(): ?\DateTimeInterface
    {
        return $this->academicYear;
    }

    public function setAcademicYear(?\DateTimeInterface $academicYear): self
    {
        $this->academicYear = $academicYear;

        return $this;
    }

    /**
     * @return Collection|Subject[]
     */
    public function getSubjects(): Collection
    {
        return $this->subjects;
    }

    public function addSubject(Subject $subject): self
    {
        if (!$this->subjects->contains($subject)) {
            $this->subjects[] = $subject;
            $subject->addCourse($this);
        }

        return $this;
    }

    public function removeSubject(Subject $subject): self
    {
        if ($this->subjects->removeElement($subject)) {
            $subject->removeCourse($this);
        }

        return $this;
    }

    public function setSubject($subjects): Course
    {
        $this->subjects = new ArrayCollection();

        foreach ($subjects as $subject) {
            $this->addSubject($subject);
        }

        return $this;
    }

    /**
     * Generates the magic method
     *
     */
    public function __toString() {
        return $this->level . " " . $this->degree;
    }
}
