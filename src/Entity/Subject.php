<?php

namespace App\Entity;

use App\Repository\SubjectRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * @ORM\Entity(repositoryClass=SubjectRepository::class)
 */
class Subject
{

    const ANNUAL = 'annual';
    const QUARTERLY = 'quarterly';
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity=Course::class, inversedBy="subjects")
     * @JoinTable(name="subject_course",
     *      joinColumns={@JoinColumn(name="subject_id", referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="course_id", referencedColumnName="id")}
     *      )
     *
     *
     */
    private Collection $course;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $credits;

    /**
     * @ORM\Column(type="string", length=255, columnDefinition="enum('annual', 'quarterly')")
     */
    private $duration;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, mappedBy="subject")
     *
     */
    private $user;

    public function __construct()
    {
        $this->course = new ArrayCollection();
        $this->user = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Course[]
     */
    public function getCourse(): Collection
    {
        return $this->course;
    }


    public function addCourse(Course $course): self
    {
        if (!$this->course->contains($course)) {
            $this->course[] = $course;
        }

        return $this;
    }

    public function removeCourse(Course $course): self
    {
        $this->course->removeElement($course);

        return $this;
    }

    public function setCourse($courses): Subject
    {
        $this->course = new ArrayCollection();

        foreach ($courses as $course) {
            $this->addCourse($course);
        }

        return $this;
    }

    public function getCredits(): ?int
    {
        return $this->credits;
    }

    public function setCredits(?int $credits): self
    {
        $this->credits = $credits;

        return $this;
    }

    public function getDuration(): ?string
    {
        return $this->duration;
    }

    public function setDuration(string $duration): self
    {
        if (!in_array($duration, array(self::ANNUAL, self::QUARTERLY))) {
            throw new \InvalidArgumentException("Invalid duration");
        }
        $this->duration = $duration;

        return $this;
    }

    /**
     * Generates the magic method
     *
     */
    public function __toString() {
        return $this->name;
    }

    /**
     * @return Collection|User[]
     */
    public function getUser(): Collection
    {
        return $this->user;
    }

    public function addUser(User $user): self
    {
        if (!$this->user->contains($user)) {
            $this->user[] = $user;
            $user->addSubject($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->user->removeElement($user)) {
            $user->removeSubject($this);
        }

        return $this;
    }
}
