<?php

namespace App\Form;

use App\Entity\Course;
use App\Entity\Subject;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CourseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('level')
            ->add('degree')
            ->add('month_duration', IntegerType::class, ['attr' => ['min' => 1, 'max' => 9,]])
            ->add('academic_year', DateType::class, array(
                'format' => 'yyyyMMdd',
                'widget' => 'choice',
                'years' => range(date('Y') - 10, date('Y') + 10),
                'months' => range(date('m'), 12),
                'days' => range(date('d'), 31),
                'label'=>'Academic year',
                'data' => new \DateTime(),
                'attr' => array(
                    'placeholder' => 'Start year, e.g., 1980 ',
                    'class' => 'form-control',
                    'style' => 'line-height: 20px;'
                ),
            ))->add('subjects', CollectionType::class, [
                'entry_type' => SubjectType::class,
                'entry_options' => [
                    'required'  => false,
                    'attr' => array (
                        'class' => 'form-control',

                    ),
                ],
                'label' => false,
                'allow_delete' => true,
                'allow_add' => true,
                'by_reference' => false,

            ]);
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Course::class,
        ]);
    }
}
