<?php

namespace App\Form;

use App\Entity\Subject;
use App\Entity\Course;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SubjectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('credits', IntegerType::class, ['attr' => ['min' => 1]])
            ->add('duration', ChoiceType::class, [
                'choices' => array(
                    'annual' => 'annual',
                    'quarterly' => 'quarterly',
                ),
                'attr' => array('class' => 'dropdown')])
            ->add('course', EntityType::class, array(
                'class'     => Course::class,
                'expanded'  => false,
                'multiple'  => true,
                'required' => false
            ));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Subject::class,
        ]);
    }
}
